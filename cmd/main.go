package main

import (
	"gitlab.com/alisherkuttybai/audit-test/internal/app"
	"gitlab.com/alisherkuttybai/audit-test/internal/config"
	"log"
)

func main() {
	cfg, err := config.New()
	if err != nil {
		log.Fatalf("failed init config: %s", err)
	}
	application, err := app.New(cfg)
	if err != nil {
		log.Fatalf("failed init application: %s", err)
	}
	application.Run()
}
