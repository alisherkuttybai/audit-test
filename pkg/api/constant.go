package api

const (
	// logger level
	LogLocal = "local"
	LogDev   = "dev"
	LogProd  = "prod"

	// status code
	Success             = 0
	BadRequest          = 1
	InternalServerError = 2
	NotFound            = 3
	MethodNotAllowed    = 4
)
