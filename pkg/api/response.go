package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type BaseResponse struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func ResponseOK(c *gin.Context, data interface{}, msgs ...string) {
	message := "OK"
	if len(msgs) > 0 {
		message = msgs[0]
	}

	ResponseGin(c, http.StatusOK, &BaseResponse{
		Code:    Success,
		Message: message,
		Data:    data,
	})
}

func ResponseBadRequest(c *gin.Context, message string) {
	ResponseGin(c, http.StatusBadRequest, &BaseResponse{
		Code:    BadRequest,
		Message: message,
	})
}

func ResponseInternalServerError(c *gin.Context, message string) {
	ResponseGin(c, http.StatusInternalServerError, &BaseResponse{
		Code:    InternalServerError,
		Message: message,
	})
}

func ResponseNotFound(c *gin.Context, message string) {
	ResponseGin(c, http.StatusNotFound, &BaseResponse{
		Code:    NotFound,
		Message: message,
	})
}

func ResponseMethodNotAllowed(c *gin.Context, message string) {
	ResponseGin(c, http.StatusMethodNotAllowed, &BaseResponse{
		Code:    MethodNotAllowed,
		Message: message,
	})
}

func ResponseGin(c *gin.Context, httpStatus int, response *BaseResponse) {
	c.JSON(httpStatus, response)
}
