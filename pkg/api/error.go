package api

import (
	"errors"
)

var (
	// logger errors
	ErrNotValidType = errors.New("not valid logger type")

	// repository errors
	ErrNotFound     = errors.New("not found")
	ErrExists       = errors.New("already exists")
	ErrInvalidInput = errors.New("invalid input")
)
