package kafka

import (
	"gitlab.com/alisherkuttybai/audit-test/internal/config"

	"github.com/segmentio/kafka-go"
)

type KafkaMQ struct {
	Reader *kafka.Reader
}

func New(cfg *config.Config) *KafkaMQ {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:   cfg.Kafka.Brokers,
		Topic:     cfg.Kafka.Topic,
		Partition: 0,
		MaxBytes:  10e6,
	})
	return &KafkaMQ{
		Reader: r,
	}
}

func (mq *KafkaMQ) Stop() error {
	return mq.Reader.Close()
}
