package logger

import (
	"log/slog"
	"os"
	"strings"

	"gitlab.com/alisherkuttybai/audit-test/internal/config"
	"gitlab.com/alisherkuttybai/audit-test/pkg/api"
	"gitlab.com/alisherkuttybai/audit-test/pkg/logger/slogpretty"
)

func New(cfg *config.Config) (*slog.Logger, error) {
	var (
		logger   *slog.Logger
		logLevel = strings.ToLower(cfg.App.LogLevel)
	)
	switch logLevel {
	case api.LogLocal:
		logger = setupPrettySlog()
	case api.LogDev:
		logger = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case api.LogProd:
		logger = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
		)
	default:
		return nil, api.ErrNotValidType
	}

	return logger, nil
}

func setupPrettySlog() *slog.Logger {
	options := slogpretty.PrettyHandlerOptions{
		SlogOpts: &slog.HandlerOptions{
			Level: slog.LevelDebug,
		},
	}

	handler := options.NewPrettyHandler(os.Stdout)

	return slog.New(handler)
}

func Err(err error) slog.Attr {
	return slog.Attr{
		Key:   "error",
		Value: slog.StringValue(err.Error()),
	}
}
