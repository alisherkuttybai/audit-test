package middleware

import (
	"context"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type key int

// TODO: change ctxKeyRequestID
const (
	ctxKeyRequestID key = 1
)

func SetRequestID() gin.HandlerFunc {
	return func(c *gin.Context) {
		id := uuid.New().String()
		c.Writer.Header().Set("X-Request-ID", id)
		ctx := context.WithValue(c.Request.Context(), ctxKeyRequestID, id)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}

func LogRequest(logger *slog.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		log := logger.With(
			slog.String("method", c.Request.Method+c.Request.RequestURI),
			slog.String("remote_adr", c.Request.RemoteAddr),
			slog.String("request_id", fmt.Sprint(c.Request.Context().Value(ctxKeyRequestID))),
		)

		start := time.Now()
		c.Next()

		log.Info(
			"request completed",
			slog.String("remote_adr", c.Request.RemoteAddr),
			slog.Int("status code", c.Writer.Status()),
			slog.String("message", http.StatusText(c.Writer.Status())),
			slog.Int64("duration in ms", time.Since(start).Milliseconds()),
		)
	}
}
