package httpserver

import (
	"fmt"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/cors"

	"github.com/gin-gonic/gin"
	"gitlab.com/alisherkuttybai/audit-test/internal/config"
	"gitlab.com/alisherkuttybai/audit-test/pkg/api"
)

type Server struct {
	HttpServer *HttpServer
	PromServer *HttpServer
}

type HttpServer struct {
	Server *http.Server
	Router *gin.Engine
}

func New(cfg *config.Config) *Server {
	if cfg.App.LogLevel == api.LogProd {
		gin.SetMode(gin.ReleaseMode)
	}

	hsrv := &HttpServer{
		Router: gin.New(),
	}

	hsrv.Server = &http.Server{
		Addr:    fmt.Sprintf("%s:%s", cfg.App.Host, cfg.App.Port),
		Handler: cors.AllowAll().Handler(hsrv),
	}

	psrv := initPromHttp(cfg)

	return &Server{
		HttpServer: hsrv,
		PromServer: psrv,
	}
}

func (srv *HttpServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	srv.Router.ServeHTTP(w, r)
}

func initPromHttp(cfg *config.Config) *HttpServer {
	psrv := &HttpServer{
		Router: gin.New(),
	}

	psrv.Server = &http.Server{
		Addr:    fmt.Sprintf("%s:%s", cfg.Prometheus.Host, cfg.Prometheus.Port),
		Handler: cors.AllowAll().Handler(psrv),
	}

	psrv.Router.GET("/metrics", gin.WrapH(promhttp.Handler()))

	return psrv
}
