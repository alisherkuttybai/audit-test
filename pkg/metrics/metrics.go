package metrics

import (
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/alisherkuttybai/audit-test/internal/config"
)

type Metrics struct {
	Counter         *prometheus.CounterVec
	Duration        *prometheus.HistogramVec
	DBQueryDuration *prometheus.HistogramVec
}

func New(cfg *config.Config) (*Metrics, error) {
	m := &Metrics{
		Counter: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name: "total_requests",
			Help: "Total number of requests",
			ConstLabels: map[string]string{
				"app_name": cfg.App.Name,
			},
		}, []string{"method", "result"}),

		Duration: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:    "http_request_duration_seconds",
			Help:    "Duration of HTTP requests in seconds",
			Buckets: prometheus.DefBuckets,
			ConstLabels: map[string]string{
				"app_name": cfg.App.Name,
			},
		}, []string{"method"}),

		DBQueryDuration: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Name:    "db_query_duration_seconds",
			Help:    "Duration of database queries in seconds",
			Buckets: prometheus.DefBuckets,
			ConstLabels: map[string]string{
				"app_name": cfg.App.Name,
			},
		}, []string{"method"}),
	}

	if err := prometheus.Register(m.Counter); err != nil {
		return nil, fmt.Errorf("failed register counter: %w", err)
	}
	if err := prometheus.Register(m.Duration); err != nil {
		return nil, fmt.Errorf("failed register duration: %w", err)
	}
	if err := prometheus.Register(m.DBQueryDuration); err != nil {
		return nil, fmt.Errorf("failed register db duration: %w", err)
	}

	return m, nil
}
