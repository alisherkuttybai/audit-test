package scylla

import (
	"fmt"
	"strconv"

	"gitlab.com/alisherkuttybai/audit-test/internal/config"

	"github.com/gocql/gocql"
)

type ScyllaDB struct {
	Session *gocql.Session
}

func New(cfg *config.Config) (*ScyllaDB, error) {
	var err error

	c := gocql.NewCluster(cfg.DB.Host)
	c.Port, err = strconv.Atoi(cfg.DB.Port)
	if err != nil {
		return nil, fmt.Errorf("failed to convert to int: %w", err)
	}
	c.Keyspace = cfg.DB.Keyspace

	s, err := c.CreateSession()
	if err != nil {
		return nil, fmt.Errorf("failed to connect to scylla: %w", err)
	}

	return &ScyllaDB{
		Session: s,
	}, nil
}

func (s *ScyllaDB) Stop() bool {
	s.Session.Close()
	return s.Session.Closed()
}
