package audit

import "time"

type ObjectLog struct {
	ObjectName string    `json:"object_name" validate:"required"`
	YearMonth  *string   `json:"-"`
	DataCode   string    `json:"date_code" validate:"required"`
	DataTime   time.Time `json:"date_time" validate:"required"`
	FieldName  string    `json:"field_name" validate:"required"`
	OldData    string    `json:"old_data"`
	NewData    string    `json:"new_data"`
	Action     string    `json:"action" validate:"required"`
	UserName   string    `json:"user_name" validate:"required"`
	UserCode   string    `json:"user_code" validate:"required"`
	Machine    string    `json:"machine" validate:"required"`
	Module     string    `json:"module" validate:"required"`
	SystemCode string    `json:"system_code" validate:"required"`
}
