package config

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	App        AppConfig        `yaml:"APP" env:"APP"`
	DB         DBConfig         `yaml:"DB" env:"DB"`
	Kafka      KafkaConfig      `yaml:"KAFKA" env:"KAFKA"`
	Prometheus PrometheusConfig `yaml:"PROMETHEUS" env:"PROMETHEUS"`
}

type AppConfig struct {
	LogLevel   string `yaml:"LOG_LEVEL" env:"APP_LOG_LEVEL" env-required:"true"`
	Name       string `yaml:"NAME" env:"APP_NAME" env-required:"true"`
	Host       string `yaml:"HOST" env:"APP_HOST" env-required:"true"`
	Port       string `yaml:"PORT" env:"APP_PORT" env-required:"true"`
	PathPrefix string `yaml:"PATH_PREFIX" env:"APP_PATH_PREFIX" env-required:"true"`
}

type DBConfig struct {
	Host     string `yaml:"HOST" env:"DB_HOST" env-required:"true"`
	Port     string `yaml:"PORT" env:"DB_PORT" env-required:"true"`
	Keyspace string `yaml:"KEYSPACE" env:"DB_KEYSPACE" env-required:"true"`
}

type KafkaConfig struct {
	Brokers []string      `yaml:"BROKERS" env:"KAFKA_BROKERS" env-required:"true"`
	Topic   string        `yaml:"TOPIC" env:"KAFKA_TOPIC" env-required:"true"`
	Timeout time.Duration `yaml:"TIMEOUT" env:"KAFKA_TIMEOUT" env-required:"true"`
}

type PrometheusConfig struct {
	Host string `yaml:"HOST" env:"PROMETHEUS_HOST" env-required:"true"`
	Port string `yaml:"PORT" env:"PROMETHEUS_PORT" env-required:"true"`
}

func New() (*Config, error) {
	cfg := &Config{}

	cfgPath := os.Getenv("CONFIG_PATH")
	if strings.TrimSpace(cfgPath) == "" {
		cfgPath = "./config/config.yaml"
	}

	if _, err := os.Stat(cfgPath); os.IsNotExist(err) {
		return nil, fmt.Errorf("config file does not exist: %s", cfgPath)
	}

	if err := cleanenv.ReadConfig("./config/config.yaml", cfg); err != nil {
		return nil, fmt.Errorf("failed read config: %w", err)
	}

	return cfg, nil
}
