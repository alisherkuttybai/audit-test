package consumer

import (
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"time"

	"github.com/go-playground/validator/v10"
	"gitlab.com/alisherkuttybai/audit-test/internal/model/audit"
	"gitlab.com/alisherkuttybai/audit-test/internal/usecase"
	"gitlab.com/alisherkuttybai/audit-test/pkg/kafka"
	"gitlab.com/alisherkuttybai/audit-test/pkg/metrics"
)

type Consumer struct {
	log      *slog.Logger
	mq       *kafka.KafkaMQ
	usecase  *usecase.UsecaseSet
	validate *validator.Validate
	metric   *metrics.Metrics
}

func New(log *slog.Logger, mq *kafka.KafkaMQ, usecase *usecase.UsecaseSet, validate *validator.Validate, m *metrics.Metrics) *Consumer {
	return &Consumer{
		log:      log,
		mq:       mq,
		usecase:  usecase,
		validate: validate,
		metric:   m,
	}
}

func (c *Consumer) Read(ctx context.Context) error {
	for {
		start := time.Now()

		m, err := c.mq.Reader.ReadMessage(ctx)
		if err != nil {
			return fmt.Errorf("failed read message from topic: %w", err)
		}
		c.log.Debug("message from topic: ", slog.String("message", string(m.Value)))

		objectLog := audit.ObjectLog{}
		if err := json.Unmarshal(m.Value, &objectLog); err != nil {
			c.log.Error("failed to unmarshal: %w", err)
			continue
		}

		if err = c.validate.Struct(&objectLog); err != nil {
			c.log.Error("failed to validate message: %w", err)
			continue
		}

		if err := c.usecase.AuditUsecase.SaveObjectLog(&objectLog); err != nil {
			c.log.Error("failed to save object log: %w", err)
			continue
		}

		duration := time.Since(start).Seconds()
		c.metric.Duration.WithLabelValues("SaveObjectLog").Observe(duration)
	}
}

func (c *Consumer) Stop() error {
	return c.mq.Reader.Close()
}
