package controller

import (
	"log/slog"

	"gitlab.com/alisherkuttybai/audit-test/internal/config"
	"gitlab.com/alisherkuttybai/audit-test/internal/controller/audit"
	"gitlab.com/alisherkuttybai/audit-test/internal/usecase"
	"gitlab.com/alisherkuttybai/audit-test/pkg/api"
	"gitlab.com/alisherkuttybai/audit-test/pkg/httpserver/middleware"
	"gitlab.com/alisherkuttybai/audit-test/pkg/metrics"

	"github.com/gin-gonic/gin"
)

type Controller struct {
	cfg             *config.Config
	log             *slog.Logger
	router          *gin.Engine
	AuditController *audit.AuditController
}

func New(cfg *config.Config, log *slog.Logger, router *gin.Engine, usecase *usecase.UsecaseSet, m *metrics.Metrics) *Controller {
	c := &Controller{
		cfg:             cfg,
		log:             log,
		router:          router,
		AuditController: audit.New(log, usecase, m),
	}
	c.setupRoutes()
	return c
}

func (c *Controller) setupRoutes() {
	srv := c.router
	srv.Use(middleware.SetRequestID())
	srv.Use(middleware.LogRequest(c.log))

	srv.NoRoute(func(c *gin.Context) {
		api.ResponseNotFound(c, "not found")
	})
	srv.NoMethod(func(c *gin.Context) {
		api.ResponseMethodNotAllowed(c, "method not allowed")
	})
	srv.GET("/health", func(c *gin.Context) {
		api.ResponseOK(c, "application is alive")
	})

	gRoute := srv.Group(c.cfg.App.PathPrefix)

	gAudit := gRoute.Group("/audit")
	gAudit.GET("/object-logs", c.AuditController.GetObjectLogs)
}
