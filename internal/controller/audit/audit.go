package audit

import (
	"log/slog"
	"time"

	"gitlab.com/alisherkuttybai/audit-test/internal/usecase"
	"gitlab.com/alisherkuttybai/audit-test/pkg/api"
	"gitlab.com/alisherkuttybai/audit-test/pkg/logger"
	"gitlab.com/alisherkuttybai/audit-test/pkg/metrics"

	"github.com/gin-gonic/gin"
)

type AuditController struct {
	log     *slog.Logger
	usecase *usecase.UsecaseSet
	metric  *metrics.Metrics
}

func New(log *slog.Logger, usecase *usecase.UsecaseSet, m *metrics.Metrics) *AuditController {
	return &AuditController{
		log:     log,
		usecase: usecase,
		metric:  m,
	}
}

func (a *AuditController) GetObjectLogs(c *gin.Context) {
	start := time.Now()

	objectName := c.Query("objectName")
	dataCode := c.Query("dataCode")
	res, err := a.usecase.AuditUsecase.GetObjectLog(objectName, dataCode)
	if err != nil {
		a.log.Error("failed get object logs", logger.Err(err))
		api.ResponseBadRequest(c, err.Error())
		a.metric.Counter.WithLabelValues("GetObjectLogs", "fail").Inc()
		return
	}

	duration := time.Since(start).Seconds()
	a.metric.Duration.WithLabelValues("GetObjectLogs").Observe(duration)

	a.metric.Counter.WithLabelValues("GetObjectLogs", "success").Inc()

	api.ResponseOK(c, res, "object logs successfully got")
}
