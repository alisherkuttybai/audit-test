package app

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-playground/validator/v10"
	"gitlab.com/alisherkuttybai/audit-test/internal/config"
	"gitlab.com/alisherkuttybai/audit-test/internal/consumer"
	"gitlab.com/alisherkuttybai/audit-test/internal/controller"
	"gitlab.com/alisherkuttybai/audit-test/internal/repository"
	"gitlab.com/alisherkuttybai/audit-test/internal/usecase"
	"gitlab.com/alisherkuttybai/audit-test/pkg/httpserver"
	"gitlab.com/alisherkuttybai/audit-test/pkg/kafka"
	"gitlab.com/alisherkuttybai/audit-test/pkg/logger"
	"gitlab.com/alisherkuttybai/audit-test/pkg/metrics"
	"gitlab.com/alisherkuttybai/audit-test/pkg/scylla"
)

type App struct {
	log        *slog.Logger
	db         *scylla.ScyllaDB
	metric     *metrics.Metrics
	consumer   *consumer.Consumer
	srv        *httpserver.Server
	controller *controller.Controller
}

func New(cfg *config.Config) (*App, error) {
	log, err := logger.New(cfg)
	if err != nil {
		return nil, fmt.Errorf("failed init logger: %w", err)
	}
	db, err := scylla.New(cfg)
	if err != nil {
		return nil, fmt.Errorf("failed init database: %w", err)
	}

	m, err := metrics.New(cfg)
	if err != nil {
		return nil, fmt.Errorf("failed init metrics: %w", err)
	}

	validate := validator.New()

	srv := httpserver.New(cfg)
	repositories := repository.New(log, db.Session, m)
	usecases := usecase.New(log, repositories)
	mq := kafka.New(cfg)
	consumer := consumer.New(log, mq, usecases, validate, m)
	controller := controller.New(cfg, log, srv.HttpServer.Router, usecases, m)

	return &App{
		log:        log,
		db:         db,
		metric:     m,
		consumer:   consumer,
		srv:        srv,
		controller: controller,
	}, nil
}

func (app *App) Run() {
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	app.log.Info("application starting")

	ctx := context.Background()

	go func() {
		if err := app.srv.HttpServer.Server.ListenAndServe(); err != nil {
			app.log.Error("listen server", logger.Err(err))
		}
	}()

	go func() {
		if err := app.srv.PromServer.Server.ListenAndServe(); err != nil {
			app.log.Error("listen prometheus server", logger.Err(err))
		}
	}()
	app.log.Info("application started")

	go func() {
		if err := app.consumer.Read(ctx); err != nil {
			app.log.Error("failed read messages", logger.Err(err))
			app.metric.Counter.WithLabelValues("SaveObjectLog", "fail").Inc()
		}
		app.metric.Counter.WithLabelValues("SaveObjectLog", "success").Inc()
	}()

	<-done
	app.log.Info("stopping application")
	app.Stop(ctx)

	app.log.Info("application stopped")
}

func (app *App) Stop(ctx context.Context) {
	if err := app.srv.HttpServer.Server.Shutdown(ctx); err != nil {
		app.log.Error("failed to stop server", logger.Err(err))
	}

	if err := app.srv.PromServer.Server.Shutdown(ctx); err != nil {
		app.log.Error("failed to stop prometheus server", logger.Err(err))
	}

	if check := app.db.Stop(); !check {
		app.log.Error("failed to stop database")
	}

	if err := app.consumer.Stop(); err != nil {
		app.log.Error("failed to stop consumer", logger.Err(err))
	}
}
