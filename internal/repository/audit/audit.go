package audit

import (
	"fmt"
	"log/slog"
	"time"

	"gitlab.com/alisherkuttybai/audit-test/internal/model/audit"
	"gitlab.com/alisherkuttybai/audit-test/pkg/metrics"

	"github.com/gocql/gocql"
)

type AuditRepository struct {
	log     *slog.Logger
	session *gocql.Session
	metric  *metrics.Metrics
}

func New(log *slog.Logger, session *gocql.Session, m *metrics.Metrics) *AuditRepository {
	return &AuditRepository{
		log:     log,
		session: session,
		metric:  m,
	}
}

func (r *AuditRepository) GetObjectLogs(objectName, dataCode string) ([]audit.ObjectLog, error) {
	start := time.Now()

	query := `SELECT 
		object_name, 
		year_month, 
		data_code, 
		action, 
		data_time, 
		field_name, 
		machine, 
		module, 
		new_data, 
		old_data, 
		system_code, 
		user_code, 
		user_name 
		FROM object_logs`
	// WHERE CASE
	// 	WHEN object_name AND data_code IS NOT NULL THEN TRUE
	// 	WHEN object_name IS NOT NULL THEN TRUE
	// 	WHEN data_code IS NOT NULL THEN TRUE
	// 	ELSE FALSE
	// END;`

	var args []interface{}
	if objectName != "" && dataCode != "" {
		query = fmt.Sprintf("%s WHERE object_name = ? AND data_code = ?", query)
		args = append(args, objectName, dataCode)
	} else if objectName != "" {
		query = fmt.Sprintf("%s WHERE object_name = ?", query)
		args = append(args, objectName)
	} else if dataCode != "" {
		query = fmt.Sprintf("%s WHERE data_code = ?", query)
		args = append(args, dataCode)
	}

	var logs []audit.ObjectLog
	var objectLog audit.ObjectLog

	iter := r.session.Query(query, args...).Iter()
	defer iter.Close()

	for iter.Scan(
		&objectLog.ObjectName,
		&objectLog.YearMonth,
		&objectLog.DataCode,
		&objectLog.Action,
		&objectLog.DataTime,
		&objectLog.FieldName,
		&objectLog.Machine,
		&objectLog.Module,
		&objectLog.NewData,
		&objectLog.OldData,
		&objectLog.SystemCode,
		&objectLog.UserCode,
		&objectLog.UserName) {
		logs = append(logs, objectLog)
	}

	duration := time.Since(start).Seconds()
	r.metric.DBQueryDuration.WithLabelValues("GetObjectLogs").Observe(duration)

	return logs, nil
}

func (r *AuditRepository) SaveObjectLog(log *audit.ObjectLog) error {
	start := time.Now()
	yearMonth := log.DataTime.Format("200601")

	query := `INSERT INTO object_logs (
		object_name, 
		data_code, 
		action, 
		data_time, 
		field_name, 
		machine, 
		module, 
		new_data, 
		old_data, 
		system_code, 
		user_code, 
		user_name, 
		year_month) 
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
		IF NOT EXISTS;`
	if err := r.session.Query(query,
		log.ObjectName,
		log.DataCode,
		log.Action,
		log.DataTime,
		log.FieldName,
		log.Machine,
		log.Module,
		log.NewData,
		log.OldData,
		log.SystemCode,
		log.UserCode,
		log.UserName,
		yearMonth).Exec(); err != nil {
		return fmt.Errorf("failed save object log: %w", err)
	}

	duration := time.Since(start).Seconds()
	r.metric.DBQueryDuration.WithLabelValues("SaveObjectLog").Observe(duration)

	return nil
}
