package repository

import (
	"log/slog"

	"gitlab.com/alisherkuttybai/audit-test/internal/repository/audit"
	"gitlab.com/alisherkuttybai/audit-test/pkg/metrics"

	"github.com/gocql/gocql"
)

type RepositorySet struct {
	log             *slog.Logger
	AuditRepository *audit.AuditRepository
}

func New(log *slog.Logger, session *gocql.Session, m *metrics.Metrics) *RepositorySet {
	return &RepositorySet{
		log:             log,
		AuditRepository: audit.New(log, session, m),
	}
}
