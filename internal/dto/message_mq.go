package dto

import (
	"time"

	"gitlab.com/alisherkuttybai/audit-test/internal/model/audit"
)

type MessageMQ struct {
	ObjectName string    `json:"objectName"`
	YearMonth  string    `json:"yearMonth"`
	DataCode   string    `json:"dataCode"`
	DataTime   time.Time `json:"dataTime"`
	FieldName  string    `json:"fieldName"`
	OldData    string    `json:"oldData"`
	NewData    string    `json:"newData"`
	Action     string    `json:"action"`
	UserName   string    `json:"userName"`
	UserCode   string    `json:"userCode"`
	Machine    string    `json:"machine"`
	Module     string    `json:"module"`
	SystemCode string    `json:"systemCode"`
}

func ConvertToObjectLog(msg MessageMQ) *audit.ObjectLog {
	return &audit.ObjectLog{
		ObjectName: msg.ObjectName,
		YearMonth:  &msg.YearMonth,
		DataCode:   msg.DataCode,
		DataTime:   msg.DataTime,
		FieldName:  msg.FieldName,
		OldData:    msg.OldData,
		NewData:    msg.NewData,
		Action:     msg.Action,
		UserName:   msg.UserName,
		UserCode:   msg.UserCode,
		Machine:    msg.Machine,
		Module:     msg.Module,
		SystemCode: msg.SystemCode,
	}
}
