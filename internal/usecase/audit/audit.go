package audit

import (
	"fmt"
	"log/slog"

	model "gitlab.com/alisherkuttybai/audit-test/internal/model/audit"
	"gitlab.com/alisherkuttybai/audit-test/internal/repository"
)

type AuditUsecase struct {
	log  *slog.Logger
	repo *repository.RepositorySet
}

func New(log *slog.Logger, repo *repository.RepositorySet) *AuditUsecase {
	return &AuditUsecase{
		log:  log,
		repo: repo,
	}
}

func (audit *AuditUsecase) GetObjectLog(objectName, dataCode string) ([]model.ObjectLog, error) {
	logs, err := audit.repo.AuditRepository.GetObjectLogs(objectName, dataCode)
	if err != nil {
		return nil, fmt.Errorf("failed to get object logs: %w", err)
	}

	return logs, nil
}

func (audit *AuditUsecase) SaveObjectLog(log *model.ObjectLog) error {
	if err := audit.repo.AuditRepository.SaveObjectLog(log); err != nil {
		return fmt.Errorf("failed to save object log: %w", err)
	}

	audit.log.Debug("object log saved")
	return nil
}
