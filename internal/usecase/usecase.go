package usecase

import (
	"gitlab.com/alisherkuttybai/audit-test/internal/repository"
	"gitlab.com/alisherkuttybai/audit-test/internal/usecase/audit"
	"log/slog"
)

type UsecaseSet struct {
	log       *slog.Logger
	AuditUsecase *audit.AuditUsecase
}

func New(logger *slog.Logger, repo *repository.RepositorySet) *UsecaseSet {
	return &UsecaseSet{
		log:       logger,
		AuditUsecase: audit.New(logger, repo),
	}
}
